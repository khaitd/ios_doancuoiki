//
//  HoaDon.swift
//  QLKS_Version1.1
//
//  Created by Lê Xuân Kha on 12/13/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class HoaDon: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var btThanhToan: UIButton!
    @IBOutlet weak var tbTongTien: UITextField!
    @IBOutlet weak var lbTenPhong: UILabel!
    @IBOutlet weak var lbThuNgan: UILabel!
    @IBOutlet weak var lbNgayRa: UILabel!
    @IBOutlet weak var lbNgayVao: UILabel!
    @IBOutlet weak var lbSoHoaDon: UILabel!
    @IBOutlet weak var tableView_hoaDon: UITableView!
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
