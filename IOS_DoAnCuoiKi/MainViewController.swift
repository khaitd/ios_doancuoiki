//
//  MainView.swift
//  QLKhachSan
//
//  Created by Lê Xuân Kha on 12/1/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit
class mainView: UIViewController, passData
{
    @IBOutlet weak var button_QLphong: UIButton!
    @IBOutlet weak var labelName: UIButton!
    @IBOutlet weak var labelAddress: UIButton!
    var name:String="Khách sạn iODroid"
    var address:String="Ngõ 96, Trần Duy Hưng"
    
    func getData(a: String, b: String) {
        self.name=a;
        self.address=b;
        self.labelName.setTitle(name, for: .normal)
        self.labelAddress.setTitle(address, for: .normal)
    }
   
    
    @IBAction func editName(_ sender: Any) {
        performSegue(withIdentifier: "editInfo", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="editInfo"){
            let subView=segue.destination as! ViewController
            subView.delegate=self
            subView.name=(labelName.titleLabel?.text)!
            subView.address=(labelAddress.titleLabel?.text)!
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelName.setTitle(name, for: .normal)
        self.labelAddress.setTitle(address, for: .normal)
    }
   
}
