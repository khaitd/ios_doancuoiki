//
//  ViewController.swift
//  QLKS_QLITBI
//
//  Created by Lê Xuân Kha on 12/12/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class ThietBi_Phong_ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var ThietBi_Phong_collection: UICollectionView!
    var phong : [String] = ["p101","p102","p103","p104","p101","p102","p103","p104","p101","p102","p103","p104","p101","p102","p103","p104","p101","p102","p103","p104"]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return phong.count
 }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! myCell
        cell.RoomID.text = phong[indexPath.row]
        cell.myIMG.image = UIImage(named: "door.png")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       ThietBi_Phong_collection.dataSource = self
        ThietBi_Phong_collection.delegate = self
    }


}

