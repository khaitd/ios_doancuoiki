//
//  PhongViewController.swift
//  QLKS_Version1.1
//
//  Created by Lê Xuân Kha on 12/12/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class PhongTrongViewController: UIViewController {

    @IBOutlet weak var button_Ngaysinh: UIButton!
    @IBOutlet weak var button_Ngaydatphong: UIButton!
    
    //@IBOutlet weak var button_ngaydatphong: UIButton!
  
    @IBOutlet weak var datepicker_ngaysinh: UIDatePicker!
    @IBOutlet weak var datepicker_ngaydatphong: UIDatePicker!
    
    
    //@IBOutlet weak var datepicker_ngaydatphong: UIDatePicker!
    
    @IBAction func touchdown_buttonNgaysinh(_ sender: Any) {
        if (datepicker_ngaysinh.isHidden == false) {
            datepicker_ngaysinh.isHidden = true
            
        }
        else {
            
            datepicker_ngaydatphong.isHidden = true
            datepicker_ngaysinh.isHidden = false
            
        }
    }
    
    @IBAction func touchdown_button_ngaydatphong(_ sender: Any) {
        if (datepicker_ngaydatphong.isHidden == false) {
            datepicker_ngaydatphong.isHidden = true
            
        }
        else {
            
            datepicker_ngaysinh.isHidden = true
            datepicker_ngaydatphong.isHidden = false
            
        }
    }
    
    
    //@IBAction func touchdown_ngaydatphong(_ sender: Any) {
        
    
    @IBAction func changevalue_daypicker_ngaysinh(_ sender: Any) {
        button_Ngaysinh.setTitle(getDate1(), for: .normal)
    }
    
    @IBAction func changevalue_daypicker_ngaydatphong(_ sender: Any) {
        button_Ngaydatphong.setTitle(getDate2(), for: .normal)
    }
    
    func getDate1()->String{
        let dateFormat=DateFormatter()
        dateFormat.dateFormat="MMM dd yyyy"
        return dateFormat.string(from: datepicker_ngaysinh.date)
    }
    func getDate2()->String{
        let dateFormat=DateFormatter()
        dateFormat.dateFormat="MMM dd yyyy"
        return dateFormat.string(from: datepicker_ngaydatphong.date)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
