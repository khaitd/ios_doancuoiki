//
//  QuanlyKhachSan_ViewController.swift
//  QLKhachSan
//
//  Created by Lê Xuân Kha on 12/4/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class Phong{
    var trangthai:Bool!=true;
}


class QuanlyKhachSan_ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = (tableView.dequeueReusableCell(withIdentifier: "cell_Phong") as! cellQLPhongTableViewCell)
        //cell.contentView.backgroundColor=UIColor.green
        if (dsPhong[indexPath.row].trangthai){
            cell.backgroundColor=UIColor.red}
        else{
            cell.backgroundColor=UIColor.green
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (!dsPhong[indexPath.row].trangthai){
            performSegue(withIdentifier: "segue_phongKhongTrong", sender: nil)
        }
        else{
            performSegue(withIdentifier: "segue_phongTrong" , sender: nil)
        }
        
    }
    var num = 0
    var arr: [Any]=[]
  
    @IBOutlet weak var table: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dsPhong.count;
    }
    
    var dsPhong:[Phong]=Array.init(repeating: Phong(), count: 10);
    
    override func viewDidLoad() {
    super.viewDidLoad()
    self.table.dataSource = self
    self.table.delegate = self
    }

}
