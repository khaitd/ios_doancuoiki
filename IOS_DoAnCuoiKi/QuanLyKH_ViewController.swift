//
//  QuanLyKH_ViewController.swift
//  QLKS_Version1.1
//
//  Created by Lê Xuân Kha on 12/13/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class QuanLyKH_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var khachhangTable: UITableView!
    
    var khachhang : [(String,String,String,String,String,String)] = Array(repeating: ("icons8-businessman.png","A (Nam)"," Khách hàng lồn","1/1/1998","0123456789","CMND: 11122233"), count: 100)
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return khachhang.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "khachhangCell", for: indexPath) as! khachhangCell
        
        cell.imageKH.image = UIImage(named: khachhang[indexPath.row].0)
        
        cell.tenKH.text = khachhang[indexPath.row].1
        cell.loaiKH.text = khachhang[indexPath.row].2
        cell.ngaysinhKH.text = khachhang[indexPath.row].3
        cell.sodtKH.text = khachhang[indexPath.row].4
        cell.cmndKH.text = khachhang[indexPath.row].5
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

   

   

        // Configure the view for the selected state
    

}
