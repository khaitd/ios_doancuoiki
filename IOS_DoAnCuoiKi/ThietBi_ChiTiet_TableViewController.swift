//
//  ThietBi_ChiTiet_TableViewController.swift
//  QLKS_QLITBI
//
//  Created by Lê Xuân Kha on 12/12/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class ThietBi_ChiTiet_TableViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    
    
    var ThietBi: [(String, String, String)] = Array(repeating: ("1","bed","ổn định"), count: 100)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ThietBi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myTableViewCell", for: indexPath) as! myTableViewCell
        cell.ID.text = ThietBi[indexPath.row].0
        cell.TenThietBi.text = ThietBi[indexPath.row].1
        cell.TinhTrang.text = ThietBi[indexPath.row].2
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete){
            ThietBi.remove(at: indexPath.row)
            ThietBi_TableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
    }
    
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        self.ThietBi_TableView.isEditing = !self.ThietBi_TableView.isEditing
        sender.title = (self.ThietBi_TableView.isEditing) ? "Done" : "Edit"    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "capNhat", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "capNhat"){
        let destination = segue.destination as! ThietBi_CapNhat_ViewController
        destination.temp = "bed"
        }
    }
    @IBOutlet weak var ThietBi_TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
ThietBi_TableView.delegate = self
        ThietBi_TableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
