//
//  PhongKhongTrongViewController.swift
//  QLKS_Version1.1
//
//  Created by Lê Xuân Kha on 12/12/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class PhongKhongTrongViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    var arr_thucAn:[String]=Array(repeating: "Gà", count: 10)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_thucAn.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView_thucAn.dequeueReusableCell(withIdentifier: "cell_phongKhongTrong")!
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            arr_thucAn.remove(at: indexPath.row);
            tableView_thucAn.deleteRows(at: [indexPath], with: .fade)
           // tableView_thucAn.reloadData()
        }
    }
   
    @IBOutlet weak var thanhToanButton: UIButton!
    @IBOutlet weak var edit_button: UIButton!
    @IBOutlet weak var tableView_thucAn: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.tableView_thucAn.setEditing(tr/, animated: true)
        tableView_thucAn.dataSource=self;
        tableView_thucAn.delegate=self;
        self.navigationItem.rightBarButtonItem=UIBarButtonItem(customView: edit_button)
        self.navigationItem.titleView=thanhToanButton
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
