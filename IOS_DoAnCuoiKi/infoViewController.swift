//
//  ViewController.swift
//  QLKhachSan
//
//  Created by Lê Xuân Kha on 12/1/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

protocol passData {
    func getData(a:String,b:String);
}
class ViewController: UIViewController {
    var delegate:passData!
    @IBOutlet weak var backbutton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var textFieldName: UITextField!
    
    @IBOutlet weak var textFieldAddress: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor=UIColor.darkGray
        backbutton.addTarget(self, action:#selector( self.goBack) , for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem=UIBarButtonItem(customView: backbutton)
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: saveButton)
        self.textFieldName.text=self.name
        self.textFieldAddress.text=self.address
    }
    var name:String=""
    var address:String=""
    
    @IBAction func saveButtonTouchDown(_ sender: Any) {
        delegate.getData(a: self.textFieldName.text!, b: self.textFieldAddress.text!)
        self.goBack()
    }
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }


}

