//
//  ThietBi_CapNhat_ViewController.swift
//  QLKS_QLITBI
//
//  Created by Lê Xuân Kha on 12/12/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class ThietBi_CapNhat_ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var tinhTrang: [String] = ["Ổn định", "Đang sửa chữa", "Hư hỏng"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tinhTrang.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return tinhTrang[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtTinhTrang.text = tinhTrang[row]
        tinhTrangPicker.isHidden = true
        txtTinhTrang.resignFirstResponder()
    }
    @IBAction func txtTinhTrangEdit(_ sender: Any) {
        tinhTrangPicker.isHidden = false
    }
    @IBOutlet weak var txtTinhTrang: UITextField!
    var temp :String = ""
    @IBOutlet weak var TenThietBi: UILabel!
    @IBOutlet weak var tinhTrangPicker: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
TenThietBi.text = temp
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
