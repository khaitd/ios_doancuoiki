//
//  DoAn_TableViewController.swift
//  QLKS_Version1.1
//
//  Created by Lê Xuân Kha on 12/13/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class DoAn_TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var DoAn : [(String,String,String,String)] = Array(repeating: ("icons8-food.png","1","pepsi","20000"), count: 100)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DoAn.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "DoAnCell", for: indexPath) as! DoAnCell
        cell.DoAnImage.image = UIImage(named:DoAn[indexPath.row].0)
        cell.ID.text = DoAn[indexPath.row].1
        cell.tenDoAn.text = DoAn[indexPath.row].2
        cell.donGia.text = DoAn[indexPath.row].3
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "capNhatDonGia", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if( segue.identifier == "capNhatDonGia"){
        let destination = segue.destination as! DoAn_CapNhatViewController
        destination.temp = "pepsi"
        }
    }

    @IBOutlet weak var DoAnTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
